# Helper variables and Make settings
.PHONY: help clean build proto-link proto-vendor run
.DEFAULT_GOAL := help
.ONESHELL :
.SHELLFLAGS := -ec
SHELL := /bin/bash

help:                                  ## Print list of tasks
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z0-9_%-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' Makefile

clean:
	rm -rf vendor
	rm -rf protobuf-import
	rm -rf pub/job.pb.go
	rm -rf build/*

proto-link:                            ## Generate go protobuf files using symlinked modules
	./protobuf-import.sh
	protoc -I ./protobuf-import -I ./ ./pub/job.proto --gofast_out=plugins=grpc:.

proto-vendor:                          ## Generate go protobuf files using go mode vendor
	go mod vendor
	protoc -I ./vendor -I ./ ./pub/job.proto --gofast_out=plugins=grpc:.

run:                                   ## Runs the generator
	./server

publish:   ## publish to docker hub ex: make publish v=latest
	docker build -t twillodotio/twillo-app-generator:$(v)  -f Dockerfile .
	docker push twillodotio/twillo-app-generator:$(v)

docker-build:       ## build with default values
		docker build -t twillodotio/twillo-app-publisher -f Dockerfile .

docker-build-prod:   ## build the for production argument gid=io.xiges.generator.webapp bp=/tmp/twillo/generator ap=5005 wp=1 gm=BF gs=ssh://git@localhost:2222/git-server/repos
		ddocker build -t twillodotio/twillo-app-publisher:$(v)  --build-arg gid=$(gid) --build-arg bp=$(bp) --build-arg ap=$(ap) --build-arg wp=$(wp) --build-arg gm=$(gm) --build-arg gs=$(gs) -f Dockerfile .

docker-publish:   ## publish to docker hub repo = twillodotio/twillo-studio ex : v=latest
	docker push twillodotio/twillo-app-publisher:$(v)

docker-run:  ## Run docker file locall
	docker run -it --network host -v /tmp/twillo/keys:/root/keys twillodotio/twillo-app-generator




