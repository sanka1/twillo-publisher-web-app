package env

/*   ############################################### Constants Values ###################################### */

const APP_PORT  = "PORT"
const BASE_PATH  = "BASE_PATH"
const WP_SIZE  = "WP" // worker pool size
const GIT_SERVER  = "GIT_SERVER"
const LOG_LEVEL  =  "LOG_LEVEL"
const SERVICE_REGISTRY  = "SR"
const GID  = "GID"
const GIT_REPOURL = "GIT_REPOURL"
/*   ############################################### System Constants ###################################### */

const defaultBasePath  = "/tmp/twillo/gen/webapp"
const defaultPort  = "5005"
const genDirectory = "/gen"
const defaultGitServerPath = "ssh://git@localhost:2222/git-server/repos"
const defaultGid ="io.xiges.generator.webapp"
const defaultPoolSize  = 1
const Project_Sufix  = "-ui"
/*   ############################################### System Variables ###################################### */

var Gid string = "io.twillo.publisher.dev.web.application-v1"
var BasePath string
var Port string
var GitServerURL string
var WorkerPoolSize int
var LogLevel string
var ServiceRegistry string
var GitRepoURL string

func SetDefaultBasePath(){
	BasePath = defaultBasePath
}

func SetDefaultGitServer()  {
	GitServerURL = defaultGitServerPath
}

func SetDefaultAppPort()  {
	Port = defaultPort
}

func GetGenDirectory() string {

	return BasePath + genDirectory
}

func SetDefaultGeneratorId() {
	Gid = defaultGid
}

func SetDefaultPoolSize()  {
	WorkerPoolSize = defaultPoolSize
}



func GetServerRepoUrl(repoName string) string {
	return GitRepoURL + "/" + repoName+".git"
}



func GetLoglevel()string  {

	if LogLevel == ""{
		return "debug"
	}
	return LogLevel
}