package git

import (
	"main.go/pkg/console"
	"main.go/pkg/xiLogger"
)


func CloneRepo(url string, localDir string) bool {
	xiLogger.Log.Debug(" cloning started : ",url)
	arguments :=[]string{"clone", url}
	val :=console.Exe(localDir,"git",arguments)
	xiLogger.Log.Debug("command status : ",val)
	return val
}

func exeClearRootDirectory(targetDir string,branchName string)  {
	xiLogger.Log.Debug(" clear other resources int the directory : ",targetDir)
	arguments :=[]string{"reset", "--hard","origin/master"}
	val :=console.Exe(targetDir,"git",arguments)
	xiLogger.Log.Debug("command status : ",val)

	arguments =[]string{"clean", "-f","-d"}
	val =console.Exe(targetDir,"git",arguments)
	xiLogger.Log.Debug("command status : ",val)
}

func exeCheckoutBranch(targetDir string,branchName string)  {
	xiLogger.Log.Debug(" exeCheckoutBranch ")
	arguments :=[]string{"checkout","-f","-B",branchName}
	val :=console.Exe(targetDir,"git",arguments)
	if  val {
		defer exeClearRootDirectory(targetDir,branchName)
	}
	xiLogger.Log.Debug("command status : ",val)
}

func PullFromRemoteRepository(targetDir string, branchName string)  {
	xiLogger.Log.Debug(" exeGitPull ")
	arguments :=[]string{"pull", "origin",branchName}
	val :=console.Exe(targetDir,"git",arguments)
	xiLogger.Log.Debug("command status : ",val)
}

func exeGitAdd(targetDir string, branchName string)  {
	xiLogger.Log.Debug(" exeGitAdd from ",targetDir)
	arguments :=[]string{"add", branchName}
	val :=console.Exe(targetDir,"git",arguments)
	xiLogger.Log.Debug("command status : ",val)
}

func exeGitCommit(targetDir string,commitMessage string,username string)  {
	exeSetGitUser(targetDir,username)
	xiLogger.Log.Debug("command : commit ")
	arguments :=[]string{"commit", "-m", commitMessage,"--no-verify"}
	val := console.Exe(targetDir,"git",arguments)
	xiLogger.Log.Debug("command status : ",val)
}



func exeSetGitUser(targetDir string, username string)  {
	xiLogger.Log.Debug(" adding user : ", username)
	arguments :=[]string{"config", "user.name", username}
	console.Exe(targetDir,"git",arguments)

	xiLogger.Log.Debug(" adding email : ", username+"@twillo.io")
	arguments =[]string{"config", "user.email", username+"@twillo.io"}
	console.Exe(targetDir,"git",arguments)

}

func exeGitPush(targetDir string,branchName string)  {
	xiLogger.Log.Debug("exeGitpush for : ",branchName)

	arguments :=[]string{"push", "origin", branchName}
	val :=console.Exe(targetDir,"git",arguments)
	xiLogger.Log.Debug("command status : ",val)
}
