package console

import (
	"main.go/pkg/xiLogger"
	"os"
	"os/exec"
)

func Exe(targetDir string,command string,args []string) bool{
	cmd := exec.Command(command, args...)
	cmd.Dir = targetDir
	cmd.Stderr = os.Stderr
	if xiLogger.IsDebugEnabled() {
		xiLogger.Log.Debug("Location : @  ", targetDir)
		xiLogger.Log.Debug(cmd.Args)
		cmd.Stderr = os.Stdout
	}
	err := cmd.Run()
	if err != nil {
		xiLogger.Logger().Error(err)
		return false
	}
	return true
}

func ExeAync(targetDir string,command string,args []string) (bool,*exec.Cmd){
	cmd := exec.Command(command, args...)
	cmd.Dir = targetDir
	cmd.Stderr = os.Stderr
	if xiLogger.IsDebugEnabled() {
		xiLogger.Log.Debug("Location : @  ", targetDir)
		xiLogger.Log.Debug(cmd.Args)
		cmd.Stderr = os.Stdout
	}
	xiLogger.Log.Info("starting cmd : ",cmd.Args)
	err := cmd.Start()
	if err != nil {
		xiLogger.Logger().Error(err)
		return false,cmd
	}
	return true,cmd
}

