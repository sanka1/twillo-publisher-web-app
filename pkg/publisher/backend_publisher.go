package publisher

import (
	"main.go/pkg/console"
	"main.go/pkg/xiLogger"
	"os/exec"
)

type BackendProject struct {

	ProjectLocation string
	ProjectName string
}

func(bp *BackendProject) publish()(bool,*exec.Cmd){

	if bpBuild(bp.ProjectLocation,bp.ProjectName){
		result,cmd := bpStart(bp.ProjectLocation)
		return  result,cmd
	}else {
		return false,nil
	}
}


func bpBuild(projectLocation string,projectName string) bool {

	xiLogger.Log.Debug("Init module backend",)
	arguments :=[]string{"mod", "init",projectName}
	console.Exe(projectLocation,"go",arguments)

	//download dependancies go mod download
	xiLogger.Log.Debug("Get dependancies",)
	arguments =[]string{"mod", "download",projectName}
	 console.Exe(projectLocation,"go",arguments)

	/*xiLogger.Log.Debug("go sum backend",)
	arguments =[]string{"mod", "tidy"}
	console.Exe(projectLocation,"go",arguments)*/

	xiLogger.Log.Debug("Building backend",)
	arguments =[]string{"build", "-o","server", "."}
   return console.Exe(projectLocation,"go",arguments)
}

func bpStart(projectLocation string)(bool,*exec.Cmd)  {

	xiLogger.Log.Debug("stopping previous process if any",)
	arguments :=[]string{"server"}
	console.Exe(projectLocation,"pkill",arguments)

	xiLogger.Log.Debug("st backend",)
	arguments =[]string{""}
	result,cmd := console.ExeAync(projectLocation,"./server",arguments)
	return  result,cmd
}