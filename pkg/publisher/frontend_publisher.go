package publisher

import (
	"main.go/pkg/console"
	"main.go/pkg/util"
	"main.go/pkg/xiLogger"
	"os"
	"os/exec"
	"time"
)

type FrontendProject struct {
	ProjectLocation string
}

func(fe *FrontendProject) publish()(bool,*exec.Cmd){

	buildresut := feBuild(fe.ProjectLocation)
	if buildresut {
		result,cmd := feStart("server/express")
		return  result,cmd
	}
	return  buildresut ,nil
}

func feBuild(projectLocation string) bool {
// ng build --output-path dist2 --watch  --source-map=false --optimization=false --extractLicenses=false --buildOptimizer=false
// --showCircularDependencies=false --outputHashing=none

	if _, err := os.Stat(projectLocation+"/node_modules"); os.IsNotExist(err) {
		er := util.CopyFile("server/node_modules.tar.xz",projectLocation+"/node_modules.tar.xz")
		if er != nil {
			xiLogger.Log.Error("Error while coping node_modules",er)
			return false
		}
		start := time.Now()
		xiLogger.Log.Debug(" installing dependencies")
		arguments :=[]string{"-xvf","node_modules.tar.xz"}
		result := console.Exe(projectLocation,"tar",arguments)
		if !result {
			return result
		}
		xiLogger.Log.Info("extracted time : ",time.Since(start))
	}
	xiLogger.Log.Debug(" Disable all usage analytics.")
	arguments :=[]string{"analytics","off"}
    console.Exe(projectLocation,"ng",arguments)

	xiLogger.Log.Debug(" building.. .")
	start := time.Now()
	arguments =[]string{"build","--output-path","/root/server/express/build","--source-map=false","--optimization=false",
		"--extractLicenses=false","--buildOptimizer=false","--showCircularDependencies=false","--outputHashing=none"}
	result := console.Exe(projectLocation,"ng",arguments)

	 xiLogger.Log.Info("build result :",result," UI building time : ",time.Since(start).Round(time.Second))

return result
}

func feStart(serverLocation string)(bool,*exec.Cmd)  {
	start := time.Now()
	xiLogger.Log.Debug("stopping previous process if any",)
	arguments :=[]string{"node"}
	console.Exe(serverLocation,"pkill",arguments)

	xiLogger.Log.Debug("starting")
	arguments =[]string{"start"}
	result,cmd := console.ExeAync(serverLocation,"npm",arguments)
	xiLogger.Log.Info("UI publishing time : ",time.Since(start).Round(time.Second))
	return  result,cmd
}