# Setup for ssh onto github
FROM  twillodotio/depmanger as builder
WORKDIR /go/src/github.com/twillo/studio/
RUN mkdir -p /repo
COPY . .
ENV GO111MODULE=on
RUN GOOS=linux cgo_enabled=0 go build -a -o twillo .
ENTRYPOINT ["./twillo"]


FROM golang:1.12.1-alpine 
WORKDIR /go/src/build.io
RUN apk add build-base

ENV GO111MODULE=on
RUN GOOS=linux go install all

RUN addgroup -S 1024
RUN adduser root 1024

RUN apk add --update nodejs npm
RUN apk add --update npm

RUN npm config set unsafe-perm true
RUN npm install @angular/cli -g --no-cache

RUN apk add git --no-cache
RUN apk add --no-cache \
  openssh-client

ARG gid
ENV GID $gid

ARG bp
ENV BASE_PATH $bp

ARG ap
ENV PORT $ap

ARG gs
ENV GIT_SERVER $gs

ARG wp
ENV WP $wp

ARG gm
ENV GM $gm


ARG loglevel
ENV LOG_LEVEL $loglevel

WORKDIR /root
RUN mkdir -p /root/.ssh

COPY --from=builder /go/src/github.com/twillo/studio/twillo .
COPY --from=builder /go/src/github.com/twillo/studio/keys /root/keys
COPY --from=builder /go/src/github.com/twillo/studio/server /root/server
COPY --from=builder /go/src/github.com/twillo/studio/start.sh .

EXPOSE $PORT 4200
CMD ["sh", "start.sh"]

