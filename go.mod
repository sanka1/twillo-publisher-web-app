module main.go

go 1.12

require (
	github.com/dimiro1/banner v1.0.0
	github.com/dmichael/go-multicast v0.0.0-20191021152323-097bfd85720a
	github.com/golang/protobuf v1.3.2
	github.com/hhatto/gocloc v0.3.3
	github.com/labstack/echo/v4 v4.1.14
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa
	google.golang.org/grpc v1.26.0
)
