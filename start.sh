#!/bin/sh

# If there is some public key in keys folder
# then it copies its contain in authorized_keys file
if [ "$(ls -A /root/keys/)" ]; then
  cp /root/keys/id_rsa  /root/.ssh/id_rsa
  cp /root/keys/config /root/.ssh/config
  chmod 700 /root/.ssh
  chmod -R 600 /root/.ssh/*
fi


# -D flag avoids executing as a daemon
/root/twillo
