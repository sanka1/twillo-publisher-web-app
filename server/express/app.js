var createError = require('http-errors');
var proxy = require('http-proxy-middleware');
var express = require('express');
var path = require('path');
var cors = require('cors')

module.exports = app; 


var DIST_DIR = path.join(__dirname, "/build");
//var PORT = 9090;
const port = process.env.PORT;
const bch = process.env.BCH;
const bcp = process.env.BCP;
//var backend = 'http://twillo-studio-service:8080';
var backend = 'http://'+bch+':'+bcp;

var onProxyReq = function(proxyReq, req, res) {
  if (req.method == 'OPTIONS') {
    res.send(200);
  }
};

var options = {
  target:backend,
  // pathRewrite: {'^/api' : 'admin/api'},
   changeOrigin: true, // for vhosted sites, changes host header to match to target's host
   ws: true, // enable websocket proxy
   logLevel: 'info',
   onProxyReq: onProxyReq
}

var fwdProxy = proxy(options);
var app = express();
app.use('/', express.static(DIST_DIR)); // demo page
app.use('/api', fwdProxy); 
app.listen(port);

 
console.info(`==> 🌎  Open up 11111 http://localhost:${port}/ -----> ${backend}`);

 
