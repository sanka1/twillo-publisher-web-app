package main

import (
	"context"
	"encoding/hex"
	_ "github.com/dimiro1/banner/autoload"
	"github.com/dmichael/go-multicast/multicast"
	"google.golang.org/grpc"
	"io"
	"log"
	"main.go/pkg/console"
	"main.go/pkg/env"
	"main.go/pkg/util"
	"main.go/pkg/xiLogger"
	"main.go/pub"
	"math/rand"
	"net"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Generator struct {
	Id string `json:"id"`
	Port           string `json:"port"`
	Workerpoolsize int    `json:"workerpoolsize"`
}

const (
	defaultMulticastAddress = "239.0.0.0:9999"
)

var mux sync.Mutex
type server struct{}
var g Generator

func init()  {

	xiLogger.Log.Info("WebApp source Generator starting .." )

	readEnvs()
	validateAndSetEnvs()
	g = Generator{}
	g.Port = env.Port
	g.Workerpoolsize = env.WorkerPoolSize

	//	go register(&g,regServer)
	go createCRPublishersPool(g.Workerpoolsize)
	go createNotifierPool(g.Workerpoolsize)
}

func readEnvs()   {
	env.Gid = os.Getenv(env.GID)
	env.Port = os.Getenv(env.APP_PORT)
	env.BasePath = os.Getenv(env.BASE_PATH)
	env.GitServerURL = os.Getenv(env.GIT_SERVER)
	env.GitRepoURL = os.Getenv(env.GIT_REPOURL)
	env.ServiceRegistry = os.Getenv(env.SERVICE_REGISTRY)

	poolsize, err := strconv.Atoi(os.Getenv(env.WP_SIZE))
	if err != nil{
		env.SetDefaultPoolSize()
	} else {
		env.WorkerPoolSize = poolsize
	}
	env.LogLevel = os.Getenv(env.LOG_LEVEL)
}

func extractServer() bool {
	xiLogger.Log.Debug(" installing dependencies")
	arguments :=[]string{"-xvf","express.tar.xz"}
	result := console.Exe("server","tar",arguments)
	if !result {
		 xiLogger.Log.Error("Error while extractinging ")
	}
	return true
}

func validateAndSetEnvs() {

	xiLogger.Log.Info("\n #################### Configuring : Environment variables ######################### ")

	if env.Port != "" {
		xiLogger.Log.Info("Port is  ::", env.Port)
	} else {
		env.SetDefaultAppPort()
		xiLogger.Log.Info("Port is not set ,proceed with default port :",env.Port)
	}
	if _, err := strconv.Atoi(env.Port); err == nil {
		xiLogger.Log.Info("validate Port  ::", env.Port)
	} else {
		xiLogger.Log.Fatal("Invalid port ", env.Port, " system shutting down")
	}

	if env.BasePath != "" {
		xiLogger.Log.Info("Base path : ", env.BasePath)
	} else {
		env.SetDefaultBasePath()
		xiLogger.Log.Info(" Default base path : ", env.BasePath)
	}

	if env.GitServerURL != "" {
		xiLogger.Log.Info(env.GIT_SERVER," : ", env.GitServerURL)
	} else {
		env.SetDefaultGitServer()
		xiLogger.Log.Info(" Default : ", env.GitServerURL)
	}


	xiLogger.Log.Info("\n #################### Configuring : Directories ######################### ")

	er := util.CreateDirectoryIfnotExits(env.BasePath)
	if er != nil {
		xiLogger.Logger().Fatal(env.BasePath, "path does not exists")
	}

	er = util.CreateDirectoryIfnotExits(env.GetGenDirectory())
	if er != nil {
		xiLogger.Logger().Fatal(env.BasePath, "path does not exists")
	}

}



func main()  {
	go grcpServer()
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	_, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
}

func grcpServer()  {
	xiLogger.Log.Info("Starting GRCP server")
	lis, err := net.Listen("tcp", ":"+g.Port)
	if err != nil {
		xiLogger.Log.Fatal("failed to listen: %v", err)
	}

	// create grpc server
	s := grpc.NewServer()
	pub.RegisterGenerateServer(s,server{})

	// and start...
	go func() {
		if err := s.Serve(lis); err != nil {
			xiLogger.Log.Fatal("failed to serve: %v", err)
		}
	}()
	time.Sleep(1 * time.Second)
	xiLogger.Log.Info("Listening ==> : ",strconv.Itoa(lis.Addr().(*net.TCPAddr).Port))
	if extractServer() {
		go register(env.Gid,strconv.Itoa(lis.Addr().(*net.TCPAddr).Port),2,5)
		multicast.Listen(defaultMulticastAddress, msgHandler)
	}
}

func msgHandler(src *net.UDPAddr, n int, b []byte) {
	xiLogger.Log.Info(n, "bytes read from", src)
	xiLogger.Log.Info(hex.Dump(b[:n]) , " 📞 Ping from the service Registry ")
	register(g.Id,g.Port,2,10)
}

func register(serviceId string,servicePort string, retryCount int , retryDelay time.Duration) {
	mux.Lock()
	defer mux.Unlock()
	var count int
	for {
		xiLogger.Log.Info(" 🔌 sending registration request")
		rand.Seed(time.Now().UnixNano())
		min := 500
		max := 5000
		val := rand.Intn(max - min + 1) + min
		time.Sleep(time.Duration(val)* time.Millisecond)
		count++
		conn, err := grpc.Dial(env.ServiceRegistry, grpc.WithInsecure(), grpc.WithBlock())
		if err != nil {
			xiLogger.Logger().Error(err)
		}

		c := pub.NewRegisterClient(conn)
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)

		r, err := c.RegisterService(ctx, &pub.RegisterRequest{Port: servicePort, ServiceId: serviceId})
		if err != nil {
			xiLogger.Logger().Error(err)
		} else {
			if strings.EqualFold(r.Port, servicePort) {
				xiLogger.Log.Info(serviceId,":",r.Port, " is registered successfully 👍")
				break
			}
		}

		if retryCount < count {
			break
		}
		er := conn.Close()
		if er != nil {
			xiLogger.Logger().Error(er)
		}
		cancel()
	}

}



func (s server) Generate(srv pub.Generate_GenerateServer) error {
	for {
		// receive data from stream
		req, err := srv.Recv()
		if err == io.EOF {
			// return will close stream from server side
			xiLogger.Log.Info("Channel closed by the client")
			return nil
		}
		if err != nil {
			log.Printf("receive error %v", err)
			continue
		}

		// continue if number reveived from stream
		// less than max
		result := jobRequestFromRPC(req,srv)
		var genResult pub.PublishResult
		if result {
			xiLogger.Log.Debug("Job submitted successfully, reply with the status : "+pub.JobStatus_ACCEPTED.String())
			genResult = pub.PublishResult{SolutionId:req.GetSolutionId(),ProjectUUId:req.GetProjectUUId(),Status:pub.JobStatus_ACCEPTED}
		} else {
			xiLogger.Log.Debug("Job Rejected, reply with the status : "+pub.JobStatus_REJECTED.String())
			genResult = pub.PublishResult{SolutionId:req.GetSolutionId(),ProjectUUId:req.GetProjectUUId(),Status:pub.JobStatus_REJECTED}
		}

		if err := srv.Send(&genResult); err != nil {
			log.Printf("send error %v", err)
		}
	}
}

func CreateDirectory(directory string) error {
	xiLogger.Log.Info("creating Directory : ",directory)
	if _, err := os.Stat(directory); os.IsNotExist(err) {

		if strings.HasPrefix( directory,"/"){
			newPath := filepath.Join(directory)
			if err := os.MkdirAll(newPath, os.ModePerm); err != nil{
				return err
			}

		}else {
			newPath := filepath.Join(".", directory)
			if err := os.MkdirAll(newPath, os.ModePerm); err != nil{
				return err
			}
		}
	}else {
		xiLogger.Log.Info("Directory already existed , Ignoring creating it : ",directory)
	}
	return nil
}
