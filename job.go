package main

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"log"
	"main.go/pkg/env"
	"main.go/pkg/git"
	"os/exec"

	"main.go/pkg/publisher"
	"main.go/pkg/xiLogger"
	"main.go/pub"

	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"

)


var backendProcess *exec.Cmd
var frontendProcess *exec.Cmd
type PublishJob struct {

	SolutionId     int    `json:"solutionId"`
	SolutionName   string `json:"solutionName"`
	ProjectUUId    string  `json:"project_uu_id"`
	ProjectName    string  `json:"projectName"`
	JobDescription string `json:"job_description"`
	PublishedUrl string `json:"published_url"`
	CallbackUrl    pub.Generate_GenerateServer

}

type PublishedResult struct {
	result pub.PublishResult
	CallbackUrl pub.Generate_GenerateServer
}

var publishedJobs = make(chan PublishJob,10)
var finishedJobs = make (chan PublishedResult,15)
//worker
func cloudRunPublisher(wg *sync.WaitGroup) {
	for job := range publishedJobs {
		 xiLogger.Log.Debug("Web App generator pic the job ")
         output := Publish(&job)
		select {
		case finishedJobs <- output:
			xiLogger.Log.Debug("Job is finished and insert the result into finish job channel ")
		default:
			xiLogger.Log.Error("Job is finished and but cannot insert the result into finish job channel ")
		}
	}
	wg.Done()
}

func createCRPublishersPool(noOfWorkers int) {
	xiLogger.Log.Info("Creating generator worker pool..." )
	var wg sync.WaitGroup
	for i := 0; i < noOfWorkers; i++ {
		wg.Add(1)
		go cloudRunPublisher(&wg)
	}
	wg.Wait()
}

func notifier(wg *sync.WaitGroup)  {

	for result := range finishedJobs {
		 xiLogger.Log.Debug(" Finished Job result , going to notify of job ID",strconv.Itoa(int(result.result.SolutionId)))
		 notify(&result)
	}
	wg.Done()
}

func createNotifierPool(noOfWorkers int) {
	xiLogger.Log.Info("Creating Notifier worker pool..." )
	var wg sync.WaitGroup
	for i := 0; i < noOfWorkers; i++ {
		wg.Add(1)
		go notifier(&wg)
	}
	wg.Wait()
}

func notify(result *PublishedResult)  {
   xiLogger.Log.Info("Sending reply with status : ",result.result.Status.String())
	if err := result.CallbackUrl.Send(&result.result); err != nil {
		xiLogger.Log.Error("send error %v", err)
	}
}



func PublisherJobRequest(job *PublishJob) bool {
	xiLogger.Log.Debug("New publish request")
	status := make(chan bool)
	go addToJobQue(job,status)
	result:= <- status
	if result {
		return true
	}else {
		return false
	}
}

func jobRequestFromREST(c echo.Context) error {

	xiLogger.Log.Debug("New Generate request")
	job := new(PublishJob)
	er1 := c.Bind(job)
	if er1 != nil {
		return c.JSON(http.StatusBadRequest,job)
	}
	result:=  PublisherJobRequest(job)
	if result {
		return c.JSON(http.StatusOK,job)
	}else {
		return c.JSON(http.StatusServiceUnavailable,job)
	}
}

func jobRequestFromRPC(job *pub.PublisherJob, srv pub.Generate_GenerateServer) bool  {

	publishJob :=new(PublishJob)
	publishJob.SolutionId = int(job.GetSolutionId())
	publishJob.SolutionName = job.GetSolutionName()
	publishJob.ProjectUUId = job.GetProjectUUId()
	publishJob.ProjectName = job.GetProjectName()
    publishJob.CallbackUrl = srv
	result:=  PublisherJobRequest(publishJob)

	return result
}

func addToJobQue(pubJob *PublishJob,respond chan<- bool) {

	select {
	case publishedJobs <- *pubJob: // Put 2 in the channel unless it is full
		log.Println(" <<<<< Inserting a Job = "+strconv.Itoa(pubJob.SolutionId))
		xiLogger.Log.Debug(" << Inserting a Job to chanel , jobId : = "+strconv.Itoa(pubJob.SolutionId))
		respond <- true
	default:
		fmt.Println("Channel full. Discarding value")
		xiLogger.Log.Warn("Couldn't insert into channel ,Channel may full. Discarding value")
		respond <- false
	}
}


func updateGit(basePath string,solutionId int,solutionName string,branchName string) (bool, string) {
	xiLogger.Log.Debug(" Getting resources from the Git At :"+branchName)
	solutionDir := basePath+"/"+strconv.Itoa(solutionId)+"_"+strings.TrimSpace(solutionName)
	projectDir := solutionDir +"/"+branchName

	if _, err := os.Stat(solutionDir); os.IsNotExist(err) {
		repoName := strconv.Itoa(solutionId)+"_"+strings.TrimSpace(solutionName)

		result := git.CloneRepo(env.GetServerRepoUrl(repoName),filepath.FromSlash(basePath))
		if !result {
			return false,""
		}
	}

	git.PullFromRemoteRepository(solutionDir,branchName)
	return true,filepath.FromSlash(projectDir)
}

func Publish(job *PublishJob) PublishedResult  {
	xiLogger.Log.Debug("Web App generator starting the job ")
	solutionDir := env.BasePath+"/"+strconv.Itoa(job.SolutionId)+"_"+strings.TrimSpace(job.SolutionName)
	xiLogger.Log.Info(" SolutionDir : ", solutionDir)
    gitBranchName :=job.ProjectUUId
	result, projectDir := updateGit(env.BasePath,job.SolutionId,job.SolutionName,gitBranchName)
    if ! result {

		genResult := pub.PublishResult{SolutionId: int32(job.SolutionId),ProjectUUId:job.ProjectUUId,
			Status:pub.JobStatus_FAILED}
		return  PublishedResult{CallbackUrl:job.CallbackUrl,result: genResult}
	}
	backendGeneratedLocation:=projectDir+"/"+job.ProjectName
	frontendGeneratedLocation:=projectDir+"/"+job.ProjectName+env.Project_Sufix

	xiLogger.Log.Info(" backendGeneratedLocation : ", backendGeneratedLocation)
	xiLogger.Log.Info(" frontendGeneratedLocation : ", frontendGeneratedLocation)
	//defer pushUpdateToGit(solutionDir,"")
//	stopPreviousProcessIfany()

	backendProject := publisher.BackendProject{ProjectLocation:backendGeneratedLocation,ProjectName:job.ProjectName}
	frontendProject := publisher.FrontendProject{ProjectLocation:frontendGeneratedLocation}
	result, bcmd := publisher.Publish(&backendProject)
	if result {
		backendProcess = bcmd
		xiLogger.Log.Info("backendProcess Id : ",backendProcess.Process.Pid)

	}else {
		genResult := pub.PublishResult{SolutionId: int32(job.SolutionId),ProjectUUId:job.ProjectUUId,
			Status:pub.JobStatus_FAILED}
		return  PublishedResult{CallbackUrl:job.CallbackUrl,result: genResult}
	}

	result, fcmd := publisher.Publish(&frontendProject)
	var jobstatus pub.JobStatus
	if result {
		frontendProcess = fcmd
		xiLogger.Log.Info("  frontendProcess Id : ",frontendProcess.Process.Pid)
		 jobstatus = pub.JobStatus_COMPLETED
	} else {
		jobstatus = pub.JobStatus_FAILED
	}

	genResult := pub.PublishResult{SolutionId: int32(job.SolutionId),ProjectUUId:job.ProjectUUId,
		Status:jobstatus}

	return  PublishedResult{CallbackUrl:job.CallbackUrl,result: genResult}
}

func stopPreviousProcessIfany() {
	 xiLogger.Log.Info("stopping previous process if any")
	if frontendProcess != nil {
		xiLogger.Log.Info("Killing frontendProcess Id : ",frontendProcess.Process.Pid)
		err := frontendProcess.Process.Kill()
		if err != nil {
			xiLogger.Logger().Error(err)
		}
	}

	if backendProcess != nil {
		xiLogger.Log.Info("Killing backendProcess Id : ",backendProcess.Process.Pid)
		err := backendProcess.Process.Kill()
		if err != nil {
			xiLogger.Logger().Error(err)
		}
	}
}





